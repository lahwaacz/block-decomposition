#include <iostream>

#include "Block.h"
#include "MultiIndex.h"
#include "decompose_1D.h"

int
main()
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    // create "global" lattice to be decomposed
    block3d global;
    global.begin = idx3d(10, -250, 0);
    global.end = idx3d(1000, 250, 250);

    // specify number of blocks
    const int num_blocks = 7;

    // do a 1D decomposition along the x-axis
    const int axis = 0;
    const std::vector<block3d> blocks = decompose_1D(global, num_blocks, axis);

    // print the results
    std::cout << "Global lattice: begin=" << global.begin
              << ", end=" << global.end << std::endl;
    for (int i = 0; i < num_blocks; i++) {
        std::cout << "Block no. " << i << ": begin=" << blocks[i].begin
                  << ", end=" << blocks[i].end << std::endl;
    }

    return EXIT_SUCCESS;
}
