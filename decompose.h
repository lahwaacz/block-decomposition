#pragma once

#include "decompose_1D.h"

/**
 * \brief Decompose a "global" block into several (sub-)blocks in a 3D manner
 * with given block counts along each axis.
 *
 * \param global The large block to decompose.
 * \param num_x Number of blocks along the x-axis.
 * \param num_y Number of blocks along the y-axis.
 * \param num_z Number of blocks along the z-axis.
 * \return A vector of the blocks into which the input was decomposed.
 */
template<typename Block>
std::vector<Block>
decompose(const Block& global,
          typename Block::idx num_x,
          typename Block::idx num_y = 1,
          typename Block::idx num_z = 1)
{
    using idx = typename Block::idx;

    std::vector<Block> result;

    for (idx block_z = 0; block_z < num_z; block_z++) {
        // split the range along the z-axis
        auto range_z =
            splitRange(global.begin.z(), global.end.z(), block_z, num_z);

        for (idx block_y = 0; block_y < num_y; block_y++) {
            // split the range along the y-axis
            auto range_y =
                splitRange(global.begin.y(), global.end.y(), block_y, num_y);

            for (idx block_x = 0; block_x < num_x; block_x++) {
                // split the range along the x-axis
                auto range_x = splitRange(
                    global.begin.x(), global.end.x(), block_x, num_x);

                // add new block and initialize it with the global size
                Block& block = result.emplace_back(global);

                // set the begin/end values for all axes
                block.begin.x() = range_x.first;
                block.end.x() = range_x.second;
                block.begin.y() = range_y.first;
                block.end.y() = range_y.second;
                block.begin.z() = range_z.first;
                block.end.z() = range_z.second;
            }
        }
    }

    return result;
}
