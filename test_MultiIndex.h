#include "catch_amalgamated.hpp"

#include "MultiIndex.h"

TEST_CASE("1D multiindex", "[Multiindex]")
{
    using idx1d = MultiIndex<1, int>;
    idx1d index = { 1 };

    SECTION("size is 1")
    {
        REQUIRE(index.getData().size() == 1);
    }

    SECTION("x component is accessible")
    {
        CHECK(index.x() == 1);
        CHECK(index[0] == 1);
    }

    SECTION("string representation")
    {
        std::stringstream str;
        str << index;
        CHECK(str.str() == "[ 1 ]");
    }

    SECTION("copy constructor")
    {
        idx1d copy(index);
        CHECK(copy.x() == index.x());
    }

    SECTION("copy-assignment operator")
    {
        idx1d copy = 2;
        copy = index;
        CHECK(copy.x() == index.x());
    }

    SECTION("elementwise comparison operators")
    {
        idx1d copy = index;
        CHECK(copy == index);
        copy.x()++;
        CHECK(copy != index);
    }

    SECTION("lexicographic comparison")
    {
        idx1d index_1 = {1};
        idx1d index_2 = {2};
        CHECK(index_1 < index_2);
        CHECK(! (index_1 < index_1));
        CHECK(! (index_2 < index_1));
    }
}

TEST_CASE("2D multiindex", "[Multiindex]")
{
    using idx2d = MultiIndex<2, int>;
    idx2d pair = { 1, 2 };

    SECTION("size is 2")
    {
        REQUIRE(pair.getData().size() == 2);
    }

    SECTION("x, y components are accessible")
    {
        CHECK(pair.x() == 1);
        CHECK(pair.y() == 2);
        CHECK(pair[0] == 1);
        CHECK(pair[1] == 2);
    }

    SECTION("string representation")
    {
        std::stringstream str;
        str << pair;
        CHECK(str.str() == "[ 1, 2 ]");
    }

    SECTION("constructor with one argument")
    {
        idx2d pair = 1;
        CHECK(pair.x() == 1);
        CHECK(pair.y() == 1);
    }

    SECTION("copy constructor")
    {
        idx2d copy(pair);
        CHECK(copy.x() == pair.x());
        CHECK(copy.y() == pair.y());
    }

    SECTION("copy-assignment operator")
    {
        idx2d copy = 2;
        copy = pair;
        CHECK(copy.x() == pair.x());
        CHECK(copy.y() == pair.y());
    }

    SECTION("elementwise comparison operators")
    {
        idx2d copy = pair;
        CHECK(copy == pair);
        copy.x()++;
        CHECK(copy != pair);
        copy = pair;
        copy.y()++;
        CHECK(copy != pair);
    }

    SECTION("lexicographic comparison")
    {
        idx2d pair_1 = {1, 1};
        idx2d pair_2 = {1, 2};
        idx2d pair_3 = {2, 1};
        CHECK(pair_1 < pair_2);
        CHECK(pair_1 < pair_3);
        CHECK(pair_2 < pair_3);
        CHECK(! (pair_1 < pair_1));
        CHECK(! (pair_2 < pair_1));
        CHECK(! (pair_3 < pair_1));
        CHECK(! (pair_3 < pair_2));
    }
}

TEST_CASE("3D multiindex", "[Multiindex]")
{
    using idx3d = MultiIndex<3, int>;
    idx3d triple = { 1, 2, 3 };

    SECTION("size is 3")
    {
        REQUIRE(triple.getData().size() == 3);
    }

    SECTION("x, y, z components are accessible")
    {
        CHECK(triple.x() == 1);
        CHECK(triple.y() == 2);
        CHECK(triple.z() == 3);
        CHECK(triple[0] == 1);
        CHECK(triple[1] == 2);
        CHECK(triple[2] == 3);
    }

    SECTION("string representation")
    {
        std::stringstream str;
        str << triple;
        CHECK(str.str() == "[ 1, 2, 3 ]");
    }

    SECTION("constructor with one argument")
    {
        idx3d triple = 1;
        CHECK(triple.x() == 1);
        CHECK(triple.y() == 1);
        CHECK(triple.z() == 1);
    }

    SECTION("copy constructor")
    {
        idx3d copy(triple);
        CHECK(copy.x() == triple.x());
        CHECK(copy.y() == triple.y());
        CHECK(copy.z() == triple.z());
    }

    SECTION("copy-assignment operator")
    {
        idx3d copy = 2;
        copy = triple;
        CHECK(copy.x() == triple.x());
        CHECK(copy.y() == triple.y());
        CHECK(copy.z() == triple.z());
    }

    SECTION("elementwise comparison operators")
    {
        idx3d copy = triple;
        CHECK(copy == triple);
        copy.x()++;
        CHECK(copy != triple);
        copy = triple;
        copy.y()++;
        CHECK(copy != triple);
        copy = triple;
        copy.z()++;
        CHECK(copy != triple);
    }

    SECTION("lexicographic comparison")
    {
        idx3d triple_1 = {1, 1, 1};
        idx3d triple_2 = {1, 2, 3};
        idx3d triple_3 = {2, 3, 1};
        CHECK(triple_1 < triple_2);
        CHECK(triple_1 < triple_3);
        CHECK(triple_2 < triple_3);
        CHECK(! (triple_1 < triple_1));
        CHECK(! (triple_2 < triple_1));
        CHECK(! (triple_3 < triple_1));
        CHECK(! (triple_3 < triple_2));
    }

    // TODO: test move-constructor, move-assignment operator
}
