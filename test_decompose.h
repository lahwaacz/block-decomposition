#include "catch_amalgamated.hpp"

#include "decompose.h"
#include "Block.h"

TEST_CASE("multidimensional decomposition", "[decompose]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    SECTION("decomposition along x-axis")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 3, 1, 1);
        REQUIRE(result.size() == 3);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(40, global.end.y(), global.end.z()));

        CHECK(result[1].begin == idx3d(40, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(70, global.end.y(), global.end.z()));

        CHECK(result[2].begin == idx3d(70, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along y-axis")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 1, 3, 1);
        REQUIRE(result.size() == 3);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), 80, global.end.z()));

        CHECK(result[1].begin == idx3d(global.begin.x(), 80, global.begin.z()));
        CHECK(result[1].end == idx3d(global.end.x(), 140, global.end.z()));

        CHECK(result[2].begin == idx3d(global.begin.x(), 140, global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along z-axis")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 1, 1, 3);
        REQUIRE(result.size() == 3);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), global.end.y(), 120));

        CHECK(result[1].begin == idx3d(global.begin.x(), global.begin.y(), 120));
        CHECK(result[1].end == idx3d(global.end.x(), global.end.y(), 210));

        CHECK(result[2].begin == idx3d(global.begin.x(), global.begin.y(), 210));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along x- and y- axes")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 3, 2, 1);
        REQUIRE(result.size() == 6);

        CHECK(result[0].begin == idx3d(global.begin.x(), global.begin.y(), global.begin.z()));
        CHECK(result[0].end == idx3d(40, 110, global.end.z()));

        CHECK(result[1].begin == idx3d(40, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(70, 110, global.end.z()));

        CHECK(result[2].begin == idx3d(70, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == idx3d(global.end.x(), 110, global.end.z()));

        CHECK(result[3].begin == idx3d(global.begin.x(), 110, global.begin.z()));
        CHECK(result[3].end == idx3d(40, global.end.y(), global.end.z()));

        CHECK(result[4].begin == idx3d(40, 110, global.begin.z()));
        CHECK(result[4].end == idx3d(70, global.end.y(), global.end.z()));

        CHECK(result[5].begin == idx3d(70, 110, global.begin.z()));
        CHECK(result[5].end == idx3d(global.end.x(), global.end.y(), global.end.z()));
    }

    SECTION("decomposition along x- and z- axes")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 3, 1, 2);
        REQUIRE(result.size() == 6);

        CHECK(result[0].begin == idx3d(global.begin.x(), global.begin.y(), global.begin.z()));
        CHECK(result[0].end == idx3d(40, global.end.y(), 165));

        CHECK(result[1].begin == idx3d(40, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(70, global.end.y(), 165));

        CHECK(result[2].begin == idx3d(70, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == idx3d(global.end.x(), global.end.y(), 165));

        CHECK(result[3].begin == idx3d(global.begin.x(), global.begin.y(), 165));
        CHECK(result[3].end == idx3d(40, global.end.y(), global.end.z()));

        CHECK(result[4].begin == idx3d(40, global.begin.y(), 165));
        CHECK(result[4].end == idx3d(70, global.end.y(), global.end.z()));

        CHECK(result[5].begin == idx3d(70, global.begin.y(), 165));
        CHECK(result[5].end == idx3d(global.end.x(), global.end.y(), global.end.z()));
    }

    SECTION("decomposition along y- and z- axes")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 1, 3, 2);
        REQUIRE(result.size() == 6);

        CHECK(result[0].begin == idx3d(global.begin.x(), global.begin.y(), global.begin.z()));
        CHECK(result[0].end == idx3d(global.end.x(), 80, 165));

        CHECK(result[1].begin == idx3d(global.begin.x(), 80, global.begin.z()));
        CHECK(result[1].end == idx3d(global.end.x(), 140, 165));

        CHECK(result[2].begin == idx3d(global.begin.x(), 140, global.begin.z()));
        CHECK(result[2].end == idx3d(global.end.x(), global.end.y(), 165));

        CHECK(result[3].begin == idx3d(global.begin.x(), global.begin.y(), 165));
        CHECK(result[3].end == idx3d(global.end.x(), 80, global.end.z()));

        CHECK(result[4].begin == idx3d(global.begin.x(), 80, 165));
        CHECK(result[4].end == idx3d(global.end.x(), 140, global.end.z()));

        CHECK(result[5].begin == idx3d(global.begin.x(), 140, 165));
        CHECK(result[5].end == idx3d(global.end.x(), global.end.y(), global.end.z()));
    }

    SECTION("decomposition along all three axes")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(100, 200, 300);

        const std::vector<block3d> result = decompose(global, 3, 3, 3);
        REQUIRE(result.size() == 27);

        CHECK(result[0].begin == idx3d(global.begin.x(), global.begin.y(), global.begin.z()));
        CHECK(result[0].end == idx3d(40, 80, 120));

        CHECK(result[1].begin == idx3d(40, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(70, 80, 120));

        CHECK(result[2].begin == idx3d(70, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == idx3d(global.end.x(), 80, 120));

        CHECK(result[3].begin == idx3d(global.begin.x(), 80, global.begin.z()));
        CHECK(result[3].end == idx3d(40, 140, 120));

        CHECK(result[4].begin == idx3d(40, 80, global.begin.z()));
        CHECK(result[4].end == idx3d(70, 140, 120));

        CHECK(result[5].begin == idx3d(70, 80, global.begin.z()));
        CHECK(result[5].end == idx3d(global.end.x(), 140, 120));

        CHECK(result[6].begin == idx3d(global.begin.x(), 140, global.begin.z()));
        CHECK(result[6].end == idx3d(40, global.end.y(), 120));

        CHECK(result[7].begin == idx3d(40, 140, global.begin.z()));
        CHECK(result[7].end == idx3d(70, global.end.y(), 120));

        CHECK(result[8].begin == idx3d(70, 140, global.begin.z()));
        CHECK(result[8].end == idx3d(global.end.x(), global.end.y(), 120));


        CHECK(result[9].begin == idx3d(global.begin.x(), global.begin.y(), 120));
        CHECK(result[9].end == idx3d(40, 80, 210));

        CHECK(result[10].begin == idx3d(40, global.begin.y(), 120));
        CHECK(result[10].end == idx3d(70, 80, 210));

        CHECK(result[11].begin == idx3d(70, global.begin.y(), 120));
        CHECK(result[11].end == idx3d(global.end.x(), 80, 210));

        CHECK(result[12].begin == idx3d(global.begin.x(), 80, 120));
        CHECK(result[12].end == idx3d(40, 140, 210));

        CHECK(result[13].begin == idx3d(40, 80, 120));
        CHECK(result[13].end == idx3d(70, 140, 210));

        CHECK(result[14].begin == idx3d(70, 80, 120));
        CHECK(result[14].end == idx3d(global.end.x(), 140, 210));

        CHECK(result[15].begin == idx3d(global.begin.x(), 140, 120));
        CHECK(result[15].end == idx3d(40, global.end.y(), 210));

        CHECK(result[16].begin == idx3d(40, 140, 120));
        CHECK(result[16].end == idx3d(70, global.end.y(), 210));

        CHECK(result[17].begin == idx3d(70, 140, 120));
        CHECK(result[17].end == idx3d(global.end.x(), global.end.y(), 210));


        CHECK(result[18].begin == idx3d(global.begin.x(), global.begin.y(), 210));
        CHECK(result[18].end == idx3d(40, 80, global.end.z()));

        CHECK(result[19].begin == idx3d(40, global.begin.y(), 210));
        CHECK(result[19].end == idx3d(70, 80, global.end.z()));

        CHECK(result[20].begin == idx3d(70, global.begin.y(), 210));
        CHECK(result[20].end == idx3d(global.end.x(), 80, global.end.z()));

        CHECK(result[21].begin == idx3d(global.begin.x(), 80, 210));
        CHECK(result[21].end == idx3d(40, 140, global.end.z()));

        CHECK(result[22].begin == idx3d(40, 80, 210));
        CHECK(result[22].end == idx3d(70, 140, global.end.z()));

        CHECK(result[23].begin == idx3d(70, 80, 210));
        CHECK(result[23].end == idx3d(global.end.x(), 140, global.end.z()));

        CHECK(result[24].begin == idx3d(global.begin.x(), 140, 210));
        CHECK(result[24].end == idx3d(40, global.end.y(), global.end.z()));

        CHECK(result[25].begin == idx3d(40, 140, 210));
        CHECK(result[25].end == idx3d(70, global.end.y(), global.end.z()));

        CHECK(result[26].begin == idx3d(70, 140, 210));
        CHECK(result[26].end == idx3d(global.end.x(), global.end.y(), global.end.z()));
    }
}
