#include <iostream>
#include <set>

#include "Block.h"
#include "MultiIndex.h"
#include "decompose.h"

template<typename Block>
void
check_result(const Block& global, const std::vector<Block>& decomposition)
{
    static_assert(Block::dimension == 3,
                  "this function works only for 3D blocks");
    using idx = typename Block::idx;

    // calculate volumes
    const idx global_volume = volume(global);
    const idx blocks_volume = volume(decomposition);

    // NOTE: these values should be equal
    std::cout << "Global volume: " << global_volume << std::endl;
    std::cout << "Volume of all blocks: " << blocks_volume << std::endl;

    // NOTE: imbalance means how much the block's volume differs from the ideal,
    // so it must be kept as close to 0 as possible
    std::cout << "Maximum imbalance of the block volumes: "
              << get_max_imbalance(decomposition) << std::endl;

    // create interior sides for blocks in the decomposition
    std::set<Block> interior_sides = create_interior_sides(decomposition);
    std::cout << "There are " << interior_sides.size() << " interior sides."
              << std::endl;

    // calculate the area of the interfaces between blocks
    // NOTE: this is the value that should be minimized
    idx interface_area = 0;
    for (const auto& side : interior_sides)
        interface_area += area(side);
    std::cout << "Total interface area is " << interface_area << std::endl;
}

int
main()
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    // create "global" lattice to be decomposed
    block3d global;
    global.begin = idx3d(0, 0, 0);
    global.end = idx3d(128, 128, 128);

    // specify numbers of blocks along each axis
    const int num_blocks_x = 4;
    const int num_blocks_y = 3;
    const int num_blocks_z = 2;

    // do a 3D decomposition
    const std::vector<block3d> blocks =
        decompose(global, num_blocks_x, num_blocks_y, num_blocks_z);

    // print the results
    std::cout << "Global lattice: begin=" << global.begin
              << ", end=" << global.end << std::endl;
    for (std::size_t i = 0; i < blocks.size(); i++) {
        std::cout << "Block no. " << i << ": begin=" << blocks[i].begin
                  << ", end=" << blocks[i].end << std::endl;
    }

    check_result(global, blocks);

    return EXIT_SUCCESS;
}
