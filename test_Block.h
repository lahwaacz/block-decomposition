#include "catch_amalgamated.hpp"

#include "Block.h"

TEST_CASE("3D block", "[Block]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    static_assert(std::is_same<typename block3d::idx, int>::value,
                  "unexpected idx type");
    static_assert(std::is_same<typename block3d::CoordinatesType, idx3d>::value,
                  "unexpected CoordinatesType");

    SECTION("default constructor")
    {
        const idx3d zero = 0;
        block3d block;
        CHECK(block.dimension == 3);
        CHECK(block.begin == zero);
        CHECK(block.end == zero);
    }

    SECTION("constructor with begin and end")
    {
        const idx3d begin = 0;
        const idx3d end = 1;
        block3d block(begin, end);
        CHECK(block.dimension == 3);
        CHECK(block.begin == begin);
        CHECK(block.end == end);
    }

    SECTION("lexicographic ordering")
    {
        block3d block_1({ 0, 1, 2 }, { 3, 4, 5 });
        block3d block_2({ 1, 2, 2 }, { 3, 4, 5 });
        block3d block_3({ 2, 3, 4 }, { 3, 4, 5 });
        CHECK(block_1 < block_2);
        CHECK(block_1 < block_3);
        CHECK(block_2 < block_3);
        CHECK(!(block_1 < block_1));
        CHECK(!(block_2 < block_1));
        CHECK(!(block_3 < block_1));
        CHECK(!(block_3 < block_2));
    }
}

TEST_CASE("1D volume", "[volume]")
{
    using idx1d = MultiIndex<1, int>;
    using block1d = Block<1, int>;

    const idx1d begin = 1;
    const idx1d end = 3;
    const block1d block(begin, end);
    CHECK(volume(block) == end.x() - begin.x());
}

TEST_CASE("2D volume", "[volume]")
{
    using idx2d = MultiIndex<2, int>;
    using block2d = Block<2, int>;

    const idx2d begin = 1;
    const idx2d end = 3;
    const block2d block(begin, end);
    CHECK(volume(block) == 2 * 2);
}

TEST_CASE("3D volume", "[volume]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    const idx3d begin = 1;
    const idx3d end = 3;
    const block3d block(begin, end);
    CHECK(volume(block) == 2 * 2 * 2);
}

TEST_CASE("volume of decomposition", "[volume]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    // this is not really a decomposition (blocks are adjacent across a vertex),
    // but still allows to test the volume function
    const idx3d p1 = 1;
    const idx3d p2 = 2;
    const idx3d p3 = 3;
    const idx3d p4 = 4;
    const std::vector<block3d> blocks = { { p1, p2 }, { p2, p3 }, { p3, p4 } };
    CHECK(volume(blocks) == 3);
}

TEST_CASE("imbalance of decomposition", "[get_max_imbalance]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    const idx3d b1_begin = { 0, 0, 0 };
    const idx3d b1_end = { 1, 1, 1 };
    const idx3d b2_begin = { 1, 0, 0 };
    const idx3d b2_end = { 2, 1, 1 };
    const idx3d b3_begin = { 2, 0, 0 };
    const idx3d b3_end = { 3, 1, 1 };
    const idx3d b4_begin = { 3, 0, 0 };
    const idx3d b4_end = { 4, 1, 1 };

    SECTION("balanced decomposition")
    {
        const std::vector<block3d> decomposition = { { b1_begin, b1_end },
                                                     { b2_begin, b2_end },
                                                     { b3_begin, b3_end },
                                                     { b4_begin, b4_end } };
        const int global_volume = volume(decomposition);
        REQUIRE(global_volume == 4);
        CHECK(get_max_imbalance(decomposition) == 0);
        CHECK(get_max_imbalance(decomposition, global_volume) == 0);
    }

    SECTION("imbalanced decomposition")
    {
        const std::vector<block3d> decomposition = { { b1_begin, b1_end },
                                                     { b2_begin, b4_end } };
        const int global_volume = volume(decomposition);
        REQUIRE(global_volume == 4);
        CHECK(get_max_imbalance(decomposition) == 0.5);
        CHECK(get_max_imbalance(decomposition, global_volume) == 0.5);
    }
}

TEST_CASE("2D block area in 3D", "[area]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    SECTION("x-normal")
    {
        const idx3d begin = { 0, 1, 2 };
        const idx3d end = { 0, 3, 4 };
        const block3d block(begin, end);
        CHECK(area(block) == 2 * 2);
    }

    SECTION("y-normal")
    {
        const idx3d begin = { 1, 0, 2 };
        const idx3d end = { 3, 0, 4 };
        const block3d block(begin, end);
        CHECK(area(block) == 2 * 2);
    }

    SECTION("z-normal")
    {
        const idx3d begin = { 1, 2, 0 };
        const idx3d end = { 3, 4, 0 };
        const block3d block(begin, end);
        CHECK(area(block) == 2 * 2);
    }
}

TEST_CASE("2D sides of a 3D block", "[create_sides]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    SECTION("one cube to vector")
    {
        const idx3d begin = { 0, 0, 0 };
        const idx3d end = { 1, 2, 3 };
        const block3d block(begin, end);

        std::vector<block3d> sides;
        create_sides(block, std::inserter(sides, sides.end()));
        REQUIRE(sides.size() == 6);
        CHECK(sides[0] == block3d{ { 0, 0, 0 }, { 1, 2, 0 } });
        CHECK(sides[1] == block3d{ { 0, 0, 0 }, { 1, 0, 3 } });
        CHECK(sides[2] == block3d{ { 0, 0, 0 }, { 0, 2, 3 } });
        CHECK(sides[3] == block3d{ { 1, 0, 0 }, { 1, 2, 3 } });
        CHECK(sides[4] == block3d{ { 0, 2, 0 }, { 1, 2, 3 } });
        CHECK(sides[5] == block3d{ { 0, 0, 3 }, { 1, 2, 3 } });
    }

    SECTION("one cube to set")
    {
        const idx3d begin = { 0, 0, 0 };
        const idx3d end = { 1, 2, 3 };
        const std::vector<block3d> blocks{ { begin, end } };

        std::set<block3d> sides_set = create_sides(blocks);
        REQUIRE(sides_set.size() == 6);

        std::vector<block3d> sides(sides_set.begin(), sides_set.end());
        CHECK(sides[0] == block3d{ { 0, 0, 0 }, { 0, 2, 3 } });
        CHECK(sides[1] == block3d{ { 0, 0, 0 }, { 1, 0, 3 } });
        CHECK(sides[2] == block3d{ { 0, 0, 0 }, { 1, 2, 0 } });
        CHECK(sides[3] == block3d{ { 0, 0, 3 }, { 1, 2, 3 } });
        CHECK(sides[4] == block3d{ { 0, 2, 0 }, { 1, 2, 3 } });
        CHECK(sides[5] == block3d{ { 1, 0, 0 }, { 1, 2, 3 } });
    }

    SECTION("two cubes to vector")
    {
        const idx3d begin_1 = { 0, 0, 0 };
        const idx3d end_1 = { 1, 2, 3 };
        const idx3d begin_2 = { 1, 0, 0 };
        const idx3d end_2 = { 2, 2, 3 };
        const std::vector<block3d> blocks{ { begin_1, end_1 },
                                           { begin_2, end_2 } };

        std::vector<block3d> sides;
        create_sides(blocks[0], std::inserter(sides, sides.end()));
        create_sides(blocks[1], std::inserter(sides, sides.end()));
        REQUIRE(sides.size() == 12);

        CHECK(sides[0] == block3d{ { 0, 0, 0 }, { 1, 2, 0 } });
        CHECK(sides[1] == block3d{ { 0, 0, 0 }, { 1, 0, 3 } });
        CHECK(sides[2] == block3d{ { 0, 0, 0 }, { 0, 2, 3 } });
        CHECK(sides[3] == block3d{ { 1, 0, 0 }, { 1, 2, 3 } });
        CHECK(sides[4] == block3d{ { 0, 2, 0 }, { 1, 2, 3 } });
        CHECK(sides[5] == block3d{ { 0, 0, 3 }, { 1, 2, 3 } });

        CHECK(sides[6] == block3d{ { 1, 0, 0 }, { 2, 2, 0 } });
        CHECK(sides[7] == block3d{ { 1, 0, 0 }, { 2, 0, 3 } });
        CHECK(sides[8] == block3d{ { 1, 0, 0 }, { 1, 2, 3 } });
        CHECK(sides[9] == block3d{ { 2, 0, 0 }, { 2, 2, 3 } });
        CHECK(sides[10] == block3d{ { 1, 2, 0 }, { 2, 2, 3 } });
        CHECK(sides[11] == block3d{ { 1, 0, 3 }, { 2, 2, 3 } });
    }

    SECTION("two cubes to set")
    {
        const idx3d begin_1 = { 0, 0, 0 };
        const idx3d end_1 = { 1, 2, 3 };
        const idx3d begin_2 = { 1, 0, 0 };
        const idx3d end_2 = { 2, 2, 3 };
        const std::vector<block3d> blocks{ { begin_1, end_1 },
                                           { begin_2, end_2 } };

        std::set<block3d> sides_set = create_sides(blocks);
        REQUIRE(sides_set.size() == 11);

        std::vector<block3d> sides(sides_set.begin(), sides_set.end());

        CHECK(sides[0] == block3d{ { 0, 0, 0 }, { 0, 2, 3 } });
        CHECK(sides[1] == block3d{ { 0, 0, 0 }, { 1, 0, 3 } });
        CHECK(sides[2] == block3d{ { 0, 0, 0 }, { 1, 2, 0 } });
        CHECK(sides[3] == block3d{ { 0, 0, 3 }, { 1, 2, 3 } });
        CHECK(sides[4] == block3d{ { 0, 2, 0 }, { 1, 2, 3 } });
        CHECK(sides[5] == block3d{ { 1, 0, 0 }, { 1, 2, 3 } });

        CHECK(sides[6] == block3d{ { 1, 0, 0 }, { 2, 0, 3 } });
        CHECK(sides[7] == block3d{ { 1, 0, 0 }, { 2, 2, 0 } });
        CHECK(sides[8] == block3d{ { 1, 0, 3 }, { 2, 2, 3 } });
        CHECK(sides[9] == block3d{ { 1, 2, 0 }, { 2, 2, 3 } });
        CHECK(sides[10] == block3d{ { 2, 0, 0 }, { 2, 2, 3 } });
    }
}

TEST_CASE("interior 2D sides of a 3D block", "[create_interior_sides]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    SECTION("two cubes")
    {
        const idx3d begin_1 = { 0, 0, 0 };
        const idx3d end_1 = { 1, 2, 3 };
        const idx3d begin_2 = { 1, 0, 0 };
        const idx3d end_2 = { 2, 2, 3 };
        const std::vector<block3d> blocks{ { begin_1, end_1 },
                                           { begin_2, end_2 } };

        std::set<block3d> sides = create_interior_sides(blocks);
        REQUIRE(sides.size() == 1);
        CHECK(*sides.begin() == block3d{ { 1, 0, 0 }, { 1, 2, 3 } });
    }
}
