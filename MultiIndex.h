#pragma once

#include <array>
#include <ostream>

template<int Size, typename Value>
class MultiIndex
{
public:
    // type of the values stored in the multi-index
    using ValueType = Value;

    // type of the array representing the multi-index
    using ArrayType = std::array<Value, Size>;

    // default constructor
    MultiIndex() = default;

    // copy constructor
    MultiIndex(const MultiIndex& other) = default;

    // move constructor
    MultiIndex(MultiIndex&& other) = default;

    // constructor which sets all components to the same value
    MultiIndex(ValueType value) { data.fill(value); }

    // constructor with arguments for each component (x, y, z, ...)
    template<typename... Ts,
             std::enable_if_t<(Size > 1) && sizeof...(Ts) == Size, bool> = true>
    MultiIndex(Ts&&... values)
      : data({ std::forward<Ts>(values)... })
    {
    }

    // copy assignment operator
    MultiIndex& operator=(const MultiIndex& other) = default;

    // move assignment operator
    MultiIndex& operator=(MultiIndex&& other) = default;

    // comparison operator
    bool operator==(const MultiIndex& other) const
    {
        return getData() == other.getData();
    }

    // comparison operator
    bool operator!=(const MultiIndex& other) const
    {
        return !operator==(other);
    }

    // getter for the underlying array
    ArrayType& getData() { return data; }

    // getter for the underlying array
    const ArrayType& getData() const { return data; }

    // getter for the first component
    Value& x()
    {
        static_assert(Size >= 1, "MultiIndex does not have x-component");
        return data[0];
    }

    // getter for the first component
    const Value& x() const
    {
        static_assert(Size >= 1, "MultiIndex does not have x-component");
        return data[0];
    }

    // getter for the second component
    Value& y()
    {
        static_assert(Size >= 2, "MultiIndex does not have y-component");
        return data[1];
    }

    // getter for the second component
    const Value& y() const
    {
        static_assert(Size >= 2, "MultiIndex does not have y-component");
        return data[1];
    }

    // getter for the third component
    Value& z()
    {
        static_assert(Size >= 3, "MultiIndex does not have z-component");
        return data[2];
    }

    // getter for the third component
    const Value& z() const
    {
        static_assert(Size >= 3, "MultiIndex does not have z-component");
        return data[2];
    }

    // generic getter for any component
    Value& operator[](std::size_t i) { return data[i]; }

    // generic getter for any component
    const Value& operator[](std::size_t i) const { return data[i]; }

private:
    ArrayType data;
};

/**
 * \brief Writes a textual representation of the given multiindex into the given
 * stream.
 */
template<int Size, typename Value>
std::ostream&
operator<<(std::ostream& str, const MultiIndex<Size, Value>& multiindex)
{
    str << "[ ";
    for (int i = 0; i < Size - 1; i++)
        str << multiindex[i] << ", ";
    str << multiindex[Size - 1];
    str << " ]";
    return str;
}

//! \brief Lexicographically compares two multiindices.
template<int Size, typename Value>
bool
operator<(const MultiIndex<Size, Value>& left,
          const MultiIndex<Size, Value>& right)
{
    return left.getData() < right.getData();
}
