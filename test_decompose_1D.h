#include "catch_amalgamated.hpp"

#include "decompose_1D.h"
#include "Block.h"

TEST_CASE("1D range splitting", "[splitRange]")
{
    SECTION("range [0, 9) split into 3 subintervals")
    {
        // result for subinterval no. 0
        const std::pair<int, int> result_0 = splitRange(0, 9, 0, 3);
        CHECK( result_0.first == 0 );
        CHECK( result_0.second == 3 );

        // result for subinterval no. 1
        const std::pair<int, int> result_1 = splitRange(0, 9, 1, 3);
        CHECK( result_1.first == 3 );
        CHECK( result_1.second == 6 );

        // result for subinterval no. 2
        const std::pair<int, int> result_2 = splitRange(0, 9, 2, 3);
        CHECK( result_2.first == 6 );
        CHECK( result_2.second == 9 );
    }

    SECTION("range [0, 10) split into 4 subintervals")
    {
        // result for subinterval no. 0
        const std::pair<int, int> result_0 = splitRange(0, 10, 0, 4);
        CHECK( result_0.first == 0 );
        CHECK( result_0.second == 3 );

        // result for subinterval no. 1
        const std::pair<int, int> result_1 = splitRange(0, 10, 1, 4);
        CHECK( result_1.first == 3 );
        CHECK( result_1.second == 6 );

        // result for subinterval no. 2
        const std::pair<int, int> result_2 = splitRange(0, 10, 2, 4);
        CHECK( result_2.first == 6 );
        CHECK( result_2.second == 8 );

        // result for subinterval no. 2
        const std::pair<int, int> result_3 = splitRange(0, 10, 3, 4);
        CHECK( result_3.first == 8 );
        CHECK( result_3.second == 10 );
    }
}

TEST_CASE("1D decomposition", "[decompose_1D]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    // create "global" lattice to be decomposed
    block3d global;
    global.begin = idx3d(10, 20, 30);
    global.end = idx3d(100, 200, 300);

    // specify number of blocks
    const int num_blocks = 3;

    SECTION("decomposition along x-axis")
    {
        const std::vector<block3d> result = decompose_1D(global, num_blocks, 0);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(40, global.end.y(), global.end.z()));

        CHECK(result[1].begin == idx3d(40, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(70, global.end.y(), global.end.z()));

        CHECK(result[2].begin == idx3d(70, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along y-axis")
    {
        const std::vector<block3d> result = decompose_1D(global, num_blocks, 1);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), 80, global.end.z()));

        CHECK(result[1].begin == idx3d(global.begin.x(), 80, global.begin.z()));
        CHECK(result[1].end == idx3d(global.end.x(), 140, global.end.z()));

        CHECK(result[2].begin == idx3d(global.begin.x(), 140, global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along z-axis")
    {
        const std::vector<block3d> result = decompose_1D(global, num_blocks, 2);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), global.end.y(), 120));

        CHECK(result[1].begin == idx3d(global.begin.x(), global.begin.y(), 120));
        CHECK(result[1].end == idx3d(global.end.x(), global.end.y(), 210));

        CHECK(result[2].begin == idx3d(global.begin.x(), global.begin.y(), 210));
        CHECK(result[2].end == global.end);
    }
}

TEST_CASE("optimal 1D decomposition", "[decompose_1D_optimal]")
{
    using idx3d = MultiIndex<3, int>;
    using block3d = Block<3, int>;

    // specify number of blocks
    const int num_blocks = 3;

    SECTION("decomposition along x-axis")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(10, 20, 30);
        global.end = idx3d(1000, 200, 300);

        const std::vector<block3d> result = decompose_1D_optimal(global, num_blocks);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(340, global.end.y(), global.end.z()));

        CHECK(result[1].begin == idx3d(340, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(670, global.end.y(), global.end.z()));

        CHECK(result[2].begin == idx3d(670, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along y-axis")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(20, 10, 30);
        global.end = idx3d(200, 1000, 300);

        const std::vector<block3d> result = decompose_1D_optimal(global, num_blocks);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), 340, global.end.z()));

        CHECK(result[1].begin == idx3d(global.begin.x(), 340, global.begin.z()));
        CHECK(result[1].end == idx3d(global.end.x(), 670, global.end.z()));

        CHECK(result[2].begin == idx3d(global.begin.x(), 670, global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along z-axis")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(20, 30, 10);
        global.end = idx3d(200, 300, 1000);

        const std::vector<block3d> result = decompose_1D_optimal(global, num_blocks);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), global.end.y(), 340));

        CHECK(result[1].begin == idx3d(global.begin.x(), global.begin.y(), 340));
        CHECK(result[1].end == idx3d(global.end.x(), global.end.y(), 670));

        CHECK(result[2].begin == idx3d(global.begin.x(), global.begin.y(), 670));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along x-axis - inverted")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(-10, -20, -30);
        global.end = idx3d(-1000, -200, -300);

        const std::vector<block3d> result = decompose_1D_optimal(global, num_blocks);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(-340, global.end.y(), global.end.z()));

        CHECK(result[1].begin == idx3d(-340, global.begin.y(), global.begin.z()));
        CHECK(result[1].end == idx3d(-670, global.end.y(), global.end.z()));

        CHECK(result[2].begin == idx3d(-670, global.begin.y(), global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along y-axis - inverted")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(-20, -10, -30);
        global.end = idx3d(-200, -1000, -300);

        const std::vector<block3d> result = decompose_1D_optimal(global, num_blocks);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), -340, global.end.z()));

        CHECK(result[1].begin == idx3d(global.begin.x(), -340, global.begin.z()));
        CHECK(result[1].end == idx3d(global.end.x(), -670, global.end.z()));

        CHECK(result[2].begin == idx3d(global.begin.x(), -670, global.begin.z()));
        CHECK(result[2].end == global.end);
    }

    SECTION("decomposition along z-axis - inverted")
    {
        // create "global" lattice to be decomposed
        block3d global;
        global.begin = idx3d(-20, -30, -10);
        global.end = idx3d(-200, -300, -1000);

        const std::vector<block3d> result = decompose_1D_optimal(global, num_blocks);
        REQUIRE(result.size() == num_blocks);

        CHECK(result[0].begin == global.begin);
        CHECK(result[0].end == idx3d(global.end.x(), global.end.y(), -340));

        CHECK(result[1].begin == idx3d(global.begin.x(), global.begin.y(), -340));
        CHECK(result[1].end == idx3d(global.end.x(), global.end.y(), -670));

        CHECK(result[2].begin == idx3d(global.begin.x(), global.begin.y(), -670));
        CHECK(result[2].end == global.end);
    }
}
