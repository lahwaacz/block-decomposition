#pragma once

#include <cmath>   // std::abs
#include <utility> // std::pair
#include <vector>

/**
 * \brief A helper function which splits a one-dimensional range.
 *
 * \param rangeBegin Beginning of the interval `[rangeBegin, rangeEnd)`.
 * \param rangeEnd End of the interval `[rangeBegin, rangeEnd)`.
 * \param rank Index of the subinterval for which the output is calculated.
 * \param num_subintervals Total number of subintervals.
 * \return A pair of indices specifying the subinterval `[begin, end)` for the
 * specified rank.
 */
template<typename Index>
std::pair<Index, Index>
splitRange(Index rangeBegin, Index rangeEnd, Index rank, Index num_subintervals)
{
    const Index rangeSize = rangeEnd - rangeBegin;
    const Index partSize = rangeSize / num_subintervals;
    const int remainder = rangeSize % num_subintervals;
    if (rank < remainder) {
        const Index begin = rangeBegin + rank * (partSize + 1);
        const Index end = begin + partSize + 1;
        return std::make_pair(begin, end);
    }
    const Index begin =
        rangeBegin + remainder * (partSize + 1) + (rank - remainder) * partSize;
    const Index end = begin + partSize;
    return std::make_pair(begin, end);
}

/**
 * \brief Decompose a "global" block into several (sub-)blocks along the given
 * axis.
 *
 * \param global The large block to decompose.
 * \param num_blocks Number of blocks in the decomposition.
 * \param axis Index of the axis (0 is x-axis, 1 is y-axis, 2 is z-axis).
 * \return A vector of the blocks into which the input was decomposed.
 */
template<typename Block>
std::vector<Block>
decompose_1D(const Block& global, typename Block::idx num_blocks, int axis = 0)
{
    using idx = typename Block::idx;

    std::vector<Block> result;

    for (idx block_idx = 0; block_idx < num_blocks; block_idx++) {
        // add new block and initialize it with the global size
        Block& block = result.emplace_back(global);

        // split the range along the specified axis
        auto range = splitRange(
            global.begin[axis], global.end[axis], block_idx, num_blocks);

        // set the begin/end values for the specified axis
        block.begin[axis] = range.first;
        block.end[axis] = range.second;
    }

    return result;
}

/**
 * \brief Find the axis for optimal 1D decomposition of a "global" block into
 * several (sub-)blocks.
 *
 * Uses the \ref decompose_1D function for the actual decomposition when the
 * axis is determined.
 *
 * \param global The large block to decompose.
 * \param num_blocks Number of blocks in the decomposition.
 * \return A vector of the blocks into which the input was decomposed.
 */
template<typename Block>
std::vector<Block>
decompose_1D_optimal(const Block& global, typename Block::idx num_blocks)
{
    using idx = typename Block::idx;

    const idx delta_x = std::abs(global.end.x() - global.begin.x());
    const idx delta_y = std::abs(global.end.y() - global.begin.y());
    const idx delta_z = std::abs(global.end.z() - global.begin.z());

    if (delta_x < delta_y) {
        if (delta_y < delta_z) {
            return decompose_1D(global, num_blocks, 2);
        }
        return decompose_1D(global, num_blocks, 1);
    }
    if (delta_x < delta_z) {
        return decompose_1D(global, num_blocks, 2);
    }
    return decompose_1D(global, num_blocks, 0);
}
