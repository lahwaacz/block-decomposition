CXX := clang++

CPPFLAGS := -MD -MP
CXXFLAGS := -std=c++17 -Wall -Wextra -pedantic -O3 -g
LDFLAGS :=

SOURCES := $(wildcard *.cpp)
OBJECTS := $(SOURCES:%.cpp=%.o)
TARGETS := v1 v3
TEST_TARGETS := test

all: $(TARGETS) $(TEST_TARGETS)

# proměnné:
# $@   target
# $^   seznam všech závislostí
# $<   první závislost ze seznamu

# pravidlo pro překlad zdrojových souborů
$(OBJECTS): %.o: %.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

# pravidlo pro linkování
$(TARGETS): % : %.o
	$(CXX) -o $@ $< $(LDFLAGS)

# pravidlo pro linkování testů (potřebujeme přidat catch_amalgamated.o)
$(TEST_TARGETS): % : %.o catch_amalgamated.o
	$(CXX) -o $@ $^ $(LDFLAGS)

clean:
	$(RM) *.[od] $(TARGETS) $(TEST_TARGETS)

-include $(SOURCES:%.cpp=%.d)
